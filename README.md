# The Refrigerated Routing Problem

This repository contains the instances and solutions for the problem presented in the paper *Extending and Solving the Refrigerated Routing Problem* by Sara Ceschia, Luca Di Gaspero and Antonella Meneghetti,  Energies, 13(23)(6214), 2020. 

## Datasets

The two datasets (**D1** and **D2**) are stored in folder
[`Datasets`](Datasets) in JSON format.

## Solutions

Best solutions are available in the folder
[`Solutions`](Solutions). 



